package com.info.hccis.cis2250_project_carver_dan;

import android.content.pm.*;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.content.Intent;
import java.util.List;

public class Tweet {

    public static Intent twitterIntent;

    public static Intent twitter(final PackageManager pm){
        String message = "I booked a court at the Spa!";

        // create a new instance of Intent called tweetIntent to send message to application (Twitter)
        twitterIntent = new Intent(Intent.ACTION_SEND);
        // get message from editText in MainActivity
        twitterIntent.putExtra(Intent.EXTRA_TEXT, message);
        // set mime type
        twitterIntent.setType("text/plain");

        // Information that is returned from resolving an intent against an IntentFilter.
        // This partially corresponds to information collected from the AndroidManifest.xml's <intent> tags.
        List<ResolveInfo> resolvedInfoList = pm.queryIntentActivities(twitterIntent, PackageManager.MATCH_DEFAULT_ONLY);
        //System.out.println(resolvedInfoList);

        // set resolved to false
        boolean resolved = false;

        // loop through returned packages until Twitter is found and set resolved to true
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.contains("com.twitter.android")){
                twitterIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if(!resolved){
            // if Twitter not installed, tweetIntent will redirect to Twitter url to allow manual post
            String url = "https://twitter.com/intent/tweet?source=webclient&text="+message;
            twitterIntent = new Intent(Intent.ACTION_VIEW);
            twitterIntent.setData(Uri.parse(url));
        }
        return twitterIntent;
    }

}
