package com.info.hccis.cis2250_project_carver_dan;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        Button buttonLogin = getView().findViewById(R.id.button2);
        buttonLogin.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                new HttpRequestTask().execute();
            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String message);
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, User> {
        String lastUser = "";
        String lastPass = "";
        SQLiteOpenHelper helper;

        @Override
        protected User doInBackground(Void... params) {
            try {
                EditText editTextUsername = getView().findViewById(R.id.editTextUser);
                EditText editTextPassword = getView().findViewById(R.id.editTextPass);
                //editTextPassword.setText(String.valueOf(lastPass));
                //editTextUsername.setText(String.valueOf(lastUser));
                String username = editTextUsername.getText().toString();
                String unhashedPassword = editTextPassword.getText().toString();
                lastUser = username;
                lastPass = unhashedPassword;

                helper = new DatabaseHelper(getActivity());

                //Read from the last time the information was saved to Shared Preferences
//                SharedPreferences sharedPref = LoginFragment.this.getActivity().getPreferences(Context.MODE_PRIVATE);
//                String defaultValue = ""; //if no last height saved
//                lastUser = sharedPref.getString(getString(R.string.username), defaultValue);

                Log.d("bjtest-lastHeight","lastHeight="+lastUser);
                //editTextUsername.setText(lastUser);


//                String defaultValue2 = ""; //if no last height saved
//                lastPass = sharedPref.getString(getString(R.string.password), defaultValue2);

                //editTextPassword.setText(lastPass);
                //editTextPassword.setText(String.valueOf(lastPass));

                if (username.isEmpty() || unhashedPassword.isEmpty()) {
                    return null;
                }

                // TEST URL: http://localhost:8080/courtbooking/rest/UserService/user/bmaclean/202cb962ac59075b964b07152d234b70

//                final String url = "http://10.0.2.2:8080/courtbooking/rest/UserService/user/" + username + "/" + md5(unhashedPassword);
                final String url = "http://hccis.info:8080/courtbooking/rest/UserService/user/" + username + "/" + md5(unhashedPassword);
                Log.d("fgotell_TEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                User user = restTemplate.getForObject(url, User.class);
                Log.d("KGTEST", "User: " + user.toString());
                return user;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        protected void onPostExecute(User user) {
            //if the returned User is not null or invalid (type code == 0) then display the data
            StringBuffer buffer = new StringBuffer();
            if (user != null && !user.getUserTypeCode().equals("0")) {
                Toast.makeText(getActivity(), "Welcome, " + user.getFirstName() + "!",
                        Toast.LENGTH_SHORT).show();

                Log.d("TEST BUFFER", "" + buffer.toString());

                SharedPreferences sharedPref = LoginFragment.this.getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.label_username), lastUser);
                editor.putString(getString(R.string.label_password), lastPass);
                editor.commit();

                Log.d("TEST BUFFER", "" + buffer.toString());
                System.out.println(buffer.toString());

                DatabaseHelper dbHelper = new DatabaseHelper(getActivity());

                Cursor temp = dbHelper.getUser(user.getUsername());

                while(temp.moveToNext()) {
                    buffer.append(temp.getString(2));
                }


                if (buffer.toString().equals(user.getUsername()))
                {

                }
                else
                {
                    Log.d("TEST BUFFER", "" + buffer.toString());
                    dbHelper.addUserRow(user.getUserId(), user.getLastName(), user.getFirstName(), user.getUsername(), md5(user.getPassword()), user.getUserTypeCode(), user.getCreatedDateTime());
                    dbHelper.getUser(user.getUsername());
                }

                FragmentManager fm = getActivity().getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                MainFragment fragment1 = new MainFragment();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment, fragment1);
                ft.commit();
            }
            else
            {
                Toast.makeText(getActivity(), "User not available",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    //    https://stackoverflow.com/questions/4846484/md5-hashing-in-android
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
