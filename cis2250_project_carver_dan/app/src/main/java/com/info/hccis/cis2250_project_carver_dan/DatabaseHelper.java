package com.info.hccis.cis2250_project_carver_dan;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "CourtBooking.db";

    //Users Table
    public static final String TABLE_NAME_USERS = "User";
    public static final String COLUMN_ID_USERS = "id";
    public static final String COLUMN_USER_NAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_LAST_NAME = "lastName";
    public static final String COLUMN_FIRST_NAME = "firstName";
    public static final String COLUMN_USER_TYPE_CODE = "userTypeCode";
    public static final String COLUMN_CREATED_DATE_TIME = "createdDateTime";


    //Booking Table
    public static final String TABLE_NAME_BOOKINGS = "Booking";
    public static final String COLUMN_ID_BOOKINGS = "bookingId";
    public static final String COLUMN_USER_ID = "userId";
    public static final String COLUMN_COURT_ID = "courtId";
    public static final String COLUMN_OPPONENT_ID = "opponentId";
    public static final String COLUMN_DATE = "dateBooked";
    public static final String COLUMN_BOOKING_START_TIME = "bookingStartTime";
    //CREATED DATE TIME
    public static final String COLUMN_CREATED_USER_ID = "createdUserId";
    public static final String COLUMN_UPDATED_DATE_TIME = "updatedDateTime";
    public static final String COLUMN_UPDATED_USER_ID = "updatedUserId";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create customers table
        String createUsersTableQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_USERS + " ( " +
                COLUMN_ID_USERS + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_USER_ID + " int(3) DEFAULT NULL," +
                COLUMN_USER_NAME + " varchar(35) DEFAULT NULL," +
                COLUMN_PASSWORD + " varchar(35) DEFAULT NULL," +
                COLUMN_FIRST_NAME + " varchar(35) DEFAULT NULL," +
                COLUMN_USER_TYPE_CODE + " varchar(35) DEFAULT NULL," +
                COLUMN_CREATED_DATE_TIME + " varchar(35) DEFAULT NULL," +
                COLUMN_LAST_NAME + " varchar(35) DEFAULT NULL);";
        db.execSQL(createUsersTableQuery);

        //Create bookings table
        String createBookingsTableQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_BOOKINGS + " ( " +
                COLUMN_ID_BOOKINGS + " int(3) DEFAULT NULL PRIMARY KEY, " +
                COLUMN_USER_ID + " int(3) DEFAULT NULL," +
                COLUMN_OPPONENT_ID + " int(3) DEFAULT NULL," +
                COLUMN_COURT_ID + " int(3) DEFAULT NULL," +
                COLUMN_BOOKING_START_TIME + " varchar(35) DEFAULT NULL," +
                COLUMN_CREATED_DATE_TIME + " varchar(35) DEFAULT NULL," +
                COLUMN_CREATED_USER_ID + " varchar(35) DEFAULT NULL," +
                COLUMN_UPDATED_DATE_TIME + " varchar(35) DEFAULT NULL," +
                COLUMN_UPDATED_USER_ID + " varchar(35) DEFAULT NULL," +
                COLUMN_DATE + " varchar(100) DEFAULT NULL);";
        db.execSQL(createBookingsTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //No present use...
    }

    public void addUserRow(int id, String lname, String fname, String user, String pass, String code, String createdDT) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_USERS, 0);
        values.put(COLUMN_USER_ID, id);
        values.put(COLUMN_LAST_NAME, lname);
        values.put(COLUMN_FIRST_NAME, fname);
        values.put(COLUMN_USER_NAME, user);
        values.put(COLUMN_PASSWORD, pass);
        values.put(COLUMN_USER_TYPE_CODE, code);
        values.put(COLUMN_CREATED_DATE_TIME, createdDT);


        db.insert(TABLE_NAME_USERS, null, values);
        db.close();
    }

    public Cursor getUser(String username)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("select * from " + TABLE_NAME_USERS + " where " + COLUMN_USER_NAME + "  = '" + username + "' LIMIT 1", null);

        return result;
    }

//    public Cursor getBooking(String id)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery("select * from " + TABLE_NAME_BOOKINGS + " where " + COLUMN_I + "  = '" + username + "' LIMIT 1", null);
//
//        return result;
//    }

    public void addBookingRow(int id,int user, int opponent, String date, int court, String startTime,
    String createdDateTime, String createdUserId,String updatedDateTime, String updatedUserId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_BOOKINGS, id);
        values.put(COLUMN_USER_ID, user);
        values.put(COLUMN_OPPONENT_ID, opponent);
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_COURT_ID, court);
        values.put(COLUMN_BOOKING_START_TIME, startTime);
        values.put(COLUMN_CREATED_DATE_TIME, createdDateTime);
        values.put(COLUMN_CREATED_USER_ID, createdUserId);
        values.put(COLUMN_UPDATED_DATE_TIME, updatedDateTime);
        values.put(COLUMN_UPDATED_USER_ID, updatedUserId);

        db.insert(TABLE_NAME_BOOKINGS, null, values);
        db.close();
    }

    public Cursor viewBookingRow(String userId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("SELECT * FROM " + TABLE_NAME_BOOKINGS + " where " + COLUMN_USER_ID + "  = '" + userId + "'", null);

        return result;
    }

    public void removeAll(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, "1", null);
    }
}

