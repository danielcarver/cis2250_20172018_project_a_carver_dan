package com.info.hccis.cis2250_project_carver_dan;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class BookingListView extends RecyclerView.ViewHolder {
    protected TextView textView;
    protected TextView textView2;

    public BookingListView(View view) {
        super(view);
        this.textView = (TextView) view.findViewById(R.id.title);
        this.textView2 = (TextView) view.findViewById(R.id.textView9);
    }
}
