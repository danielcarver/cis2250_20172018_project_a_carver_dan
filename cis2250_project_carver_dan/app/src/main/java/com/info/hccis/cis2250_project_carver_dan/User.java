package com.info.hccis.cis2250_project_carver_dan;

/**
 * Created by dcarver on 1/31/2018.
 */

public class User {
    private int userId;
    private String username;
    private String password;
    private String lastName;
    private String firstName;
    private String userTypeCode;
    private String createdDateTime;

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getUserTypeCode() {
        return userTypeCode;
    }

    public String getCreatedDateTime() { return createdDateTime; }

    @Override
    public String toString() {
        return "UserId: " + userId + ", Username: " + username;
    }
}
