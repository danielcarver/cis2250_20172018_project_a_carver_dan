package com.info.hccis.cis2250_project_carver_dan;

public class Booking {

    private int bookingId;
    private int userId;
    private int courtId;
    private int opponent;
    private String bookingDate;
    private String bookingStartTime;
    private String createdDateTime;
    private String createdUserId;
    private String updatedDateTime;
    private String updatedUserId;

    public Booking() {
    }

    public Booking(int bookingId) {
        this.bookingId = bookingId;
    }

    public Booking(int bookingId, int userId, int courtId) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.courtId = courtId;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public int getUserId() {
        return userId;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCourtId() {
        return courtId;
    }

    public void setCourtId(int courtId) {
        this.courtId = courtId;
    }

    public int getOpponent() {
        return opponent;
    }

    public void setOpponent(int opponent) {
        this.opponent = opponent;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingStartTime() {
        return bookingStartTime;
    }

    public void setBookingStartTime(String bookingStartTime) {
        this.bookingStartTime = bookingStartTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime= updatedDateTime;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Booking[ bookingId=" + bookingId + " ]";
    }

}