package com.info.hccis.cis2250_project_carver_dan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.springframework.http.HttpRequest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainFragment.OnFragmentInteractionListener, LoginFragment.OnFragmentInteractionListener,
        ViewBookingsFragment.OnFragmentInteractionListener  {

    FragmentManager fm = this.getFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    Fragment fragment1;
//    Fragment fragment2;
//    ViewBookingsFragment fragment3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        //******************************************************************************************
        // Set the initial fragment on the main activity layout
        //******************************************************************************************
        if (savedInstanceState == null) {
            SharedPreferences sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
            String defaultValue = ""; //if no last height saved
            String lastUser = sharedPref.getString(getString(R.string.label_username), defaultValue);
            String defaultValue2 = ""; //if no last height saved
            String lastPass = sharedPref.getString(getString(R.string.label_password), defaultValue2);
            StringBuffer buffer = new StringBuffer();

                if (lastUser.equals(""))
                {
                    fragment1 = new LoginFragment();
                    ft.replace(R.id.fragment, new LoginFragment());
                    ft.commit();
                }
                else
                {
                    fragment1 = new MainFragment();
                    ft.replace(R.id.fragment, new MainFragment());
                    ft.commit();

                    DatabaseHelper dbHelper = new DatabaseHelper(this);

                    Cursor temp = dbHelper.getUser(lastUser);

                    while(temp.moveToNext()) {
                        buffer.append(temp.getString(4));
                    }

                    Toast.makeText(this, "Welcome back, " + buffer.toString(),
                            Toast.LENGTH_SHORT).show();
                }
        }

        //******************************************************************************************
        //
        // Have the app change the fragment based on which element from the names array is chosen.
        //******************************************************************************************

//        final ListView lv = (ListView) findViewById(R.id.listViewNames);
//        lv.setClickable(true);
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//
//                Object o = lv.getItemAtPosition(position);
//
//                //**********************************************************************************
//                // Pop a toast
//                //**********************************************************************************
//
//                String chosen = (String) o;
//
//                Toast.makeText(getApplicationContext(),
//                        chosen, Toast.LENGTH_SHORT).show();
//
//                //**********************************************************************************
//                // Log.d example.  This debugging can be viewed in the Logcat tab in Android Studio.
//                // A way to allow you to debug your programs.
//                //**********************************************************************************
//                Log.d("BJTEST", "chosen item from listview = " + chosen);
//
//                Fragment newFragment = new FragmentMacLeanBJ();
//
//                //**********************************************************************************
//                // Use a switch to set the appropriate fragment based on the name chosen in the listview.
//                //**********************************************************************************
//
//                switch(chosen){
//                    case "Show Dialog":
//                        showDialog(MainActivity.this, "Hi", "Did this work?");
//                        newFragment = new FragmentMacLeanBJ(); //Replace this with your fragment class
//                        break;
//                    case "Joe Smith":
//                        newFragment = new FragmentMacLeanBJ(); //Replace this with your fragment class
//                        break;
//                    default:
//                        newFragment = new FragmentMacLeanBJ(); //BJs for default
//                }
//
//                //**********************************************************************************
//                // This code will replace the fragment on the content_main.xml with the new one.
//                //**********************************************************************************
//
//                ft = fm.beginTransaction();
//                ft.replace(R.id.fragment, newFragment);
//                ft.commit();
//            }
//        });
    }







    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************
            Toast.makeText(getApplicationContext(),
                    "Hi, settings was chosen", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        switch (id)
//        {
//            case 0:
//                fragment1 = new MainFragment();
//                ft.replace(R.id.fragment, new MainFragment());
//                ft.commit();
//                break;
//            case 1:
//                fragment2 = new LoginFragment();
//                ft.replace(R.id.fragment, fragment2);
//                ft.commit();
//                break;
//        }

        if (id == R.id.nav_camera) {
            Log.d("ddddddddd","TEST");
            fragment1 = new MainFragment();
            ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment1);
            ft.commit();
        } else if (id == R.id.nav_gallery) {
            Log.d("ddddddddd","TEST");
            fragment1 = new LoginFragment();
            ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment1);
            ft.commit();
        } else if (id == R.id.nav_slideshow) {
            Log.d("ddddddddd","TEST");
            fragment1 = new ViewBookingsFragment();
            ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment1);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String message) {
        Log.d("bjtest", "interaction with fragment happened: "+message);
    }
}
