package com.info.hccis.cis2250_project_carver_dan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

public class BookingRecyclerAdapter extends RecyclerView.Adapter<BookingListView> {

    List<Booking> bookingList;
    Context context;

    public BookingRecyclerAdapter(Context context, List<Booking> bookingList) {
        this.bookingList = bookingList;
        this.context = context;
    }

    @Override
    public BookingListView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_booking_list_row, null);
        return new BookingListView(view);
    }

    @Override
    public void onBindViewHolder(BookingListView holder, int position) {
        Booking booking = bookingList.get(position);

        holder.textView.setText(booking.getBookingStartTime());
        holder.textView.setOnClickListener(clickListener);
        holder.textView.setTag(holder);

        holder.textView2.setText(booking.getBookingDate());
        holder.textView2.setOnClickListener(clickListener);
        holder.textView2.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return (null != bookingList ? bookingList.size() : 0);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            BookingListView holder = (BookingListView) view.getTag();
            int position = holder.getAdapterPosition();

            Booking booking = bookingList.get(position);
        }
    };
}