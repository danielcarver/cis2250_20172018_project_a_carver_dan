package com.info.hccis.cis2250_project_carver_dan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;
import com.google.gson.Gson;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewBookingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewBookingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewBookingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SQLiteOpenHelper helper;
    HttpURLConnection urlConnection;

    private String bookingDate = "";

    private String tempUserId = "";

    //ArrayList to hold bookings
    ArrayList<Booking> bookings = new ArrayList<>();

    //Views
    RecyclerView recyclerView;
    BookingRecyclerAdapter bookingRecyclerAdapter;

    private OnFragmentInteractionListener mListener;

    public ViewBookingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViewBookingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewBookingsFragment newInstance(String param1, String param2) {
        ViewBookingsFragment fragment = new ViewBookingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_bookings, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();

        //getActivity().setContentView(R.layout.fragment_view_bookings);
        //Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.app_bar);
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.fab);
        final Tweet tweet = new Tweet();
        final PackageManager pm = getActivity().getPackageManager();
        Log.d("TEST","MADE IT");
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST","MADE IT");
//                tweet.twitter(pm);
                String message = "I booked a court at the Spa on " + bookingDate + "!";
                Intent intent = new Intent(Intent.ACTION_SEND);

                try {
                    PackageInfo pkgInfo = pm.getPackageInfo("com.twitter.android", 0);
                    String getPkgInfo = pkgInfo.toString();

                    if (getPkgInfo.equals("com.twitter.android"))
                    {
                        Log.d("TEST","TWITTER NOT INSTALLED");
                        getActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=USERID"));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();

                    // APP NOT INSTALLED
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/intent/tweet?source=webclient&text="+message));
                    startActivity(Intent.createChooser(intent, "Share With"));
                }

//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_TEXT, shareString);
//                String url = "https://twitter.com/intent/tweet?source=webclient&text=" + shareString;
//                intent.setData(Uri.parse(url));

            }
        });

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String defaultValue = ""; //if no last height saved
        String lastUser = sharedPref.getString(getString(R.string.label_username), defaultValue);

        StringBuffer buffer = new StringBuffer();
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        Cursor temp = dbHelper.getUser(lastUser);

        while(temp.moveToNext()) {
            buffer.append(temp.getString(1));
            tempUserId = buffer.toString();
        }
        helper = new DatabaseHelper(getActivity());

        new Task().execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String message);
    }

    private class Task extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
//            Looper.prepare();

            int result = 0;
            StringBuilder data = new StringBuilder();

            try {
                //Get JSON data from REST api
                URL url = new URL("http://hccis.info:8080/courtbooking/rest/BookingService/bookings/" + tempUserId);
                urlConnection = (HttpURLConnection) url.openConnection();
                int statusCode = urlConnection.getResponseCode();

                Log.d("TEST",""+statusCode);
                if (statusCode == 200) {
                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

                    //Reading the data with a Buffered Reader
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String l;
                    while ((l = reader.readLine()) != null) {
                        data.append(l);
                    }
                    processData(data.toString());
                    //Result successful
                    result = 1;
                } else {
                    //Result unsuccessful
                    result = 0;
                }
            } catch (Exception e) {
                Log.e(e.getClass().getName(), e.getMessage(), e);
            } finally {
                urlConnection.disconnect();
            }

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {

            if (result == 1) {
                bookingRecyclerAdapter = new BookingRecyclerAdapter(getActivity(), bookings);
                recyclerView.setAdapter(bookingRecyclerAdapter);
                bookingDate = bookings.get(0).getBookingDate();
            } else {
                DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
                StringBuffer buffer = new StringBuffer();
                Cursor temp = dbHelper.viewBookingRow(tempUserId);

                while(temp.moveToNext()) {
                    Booking booking = new Booking();

                    booking.setBookingId(temp.getInt(0));
                    booking.setUserId(temp.getInt(1));
                    booking.setOpponent(temp.getInt(2));
                    booking.setCourtId(temp.getInt(3));
                    booking.setBookingStartTime(temp.getString(4));
                    booking.setCreatedDateTime(temp.getString(5));
                    booking.setCreatedUserId(temp.getString(6));
                    booking.setUpdatedDateTime(temp.getString(7));
                    booking.setUpdatedUserId(temp.getString(8));
                    booking.setBookingDate(temp.getString(9));

                    bookings.add(booking);
                }
//                bookingDate = bookings.get(1).getBookingDate();
                bookingRecyclerAdapter = new BookingRecyclerAdapter(getActivity(), bookings);
                recyclerView.setAdapter(bookingRecyclerAdapter);
                bookingDate = bookings.get(0).getBookingDate();
                Toast.makeText(getActivity(), "Connection failed. Displaying old data", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Process the data being pulled from REST api
     *
     * @param data - Downloaded data to be parsed
     */
    private void processData(String data) {
        //Get JSON data into ArrayList
        try {
            DatabaseHelper dbHelper = new DatabaseHelper(getActivity());

            Gson gson = new Gson();
            Booking[] bookingsTemp = gson.fromJson(data, Booking[].class);
            for(Booking booking: bookingsTemp){
                Log.d("Added booking",""+booking);

                StringBuffer buffer = new StringBuffer();
                Cursor temp = dbHelper.viewBookingRow(tempUserId);

                bookings.add(booking);
            }
            for (Booking booking: bookings)
            {
                dbHelper.addBookingRow(booking.getBookingId(), booking.getUserId(), booking.getOpponent(), booking.getBookingDate(), booking.getCourtId(), booking.getBookingStartTime(),
                        booking.getCreatedDateTime(), booking.getCreatedUserId(), booking.getUpdatedDateTime(), booking.getUpdatedUserId());
            }
        } catch (Exception e) {
            Log.e(e.getClass().getName(), e.getMessage(), e);
        }
    }
}
