package cis2250.hccis.info.fragmentapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BlankFragment.OnFragmentInteractionListener {

    FragmentManager fm = this.getFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    BlankFragment fragment1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //******************************************************************************************
        // Set the initial fragment on the main activity layout
        //******************************************************************************************

        if (savedInstanceState == null) {
            fragment1 = new BlankFragment();
            ft.replace(R.id.fragment, new BlankFragment());
            ft.commit();
        }

        //******************************************************************************************
        //
        // Have the app change the fragment based on which element from the names array is chosen.
        //******************************************************************************************

//        final ListView lv = (ListView) findViewById(R.id.listViewNames);
//        lv.setClickable(true);
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//
//                Object o = lv.getItemAtPosition(position);
//
//                //**********************************************************************************
//                // Pop a toast
//                //**********************************************************************************
//
//                String chosen = (String) o;
//
//                Toast.makeText(getApplicationContext(),
//                        chosen, Toast.LENGTH_SHORT).show();
//
//                //**********************************************************************************
//                // Log.d example.  This debugging can be viewed in the Logcat tab in Android Studio.
//                // A way to allow you to debug your programs.
//                //**********************************************************************************
//                Log.d("BJTEST", "chosen item from listview = " + chosen);
//
//                Fragment newFragment = new FragmentMacLeanBJ();
//
//                //**********************************************************************************
//                // Use a switch to set the appropriate fragment based on the name chosen in the listview.
//                //**********************************************************************************
//
//                switch(chosen){
//                    case "Show Dialog":
//                        showDialog(MainActivity.this, "Hi", "Did this work?");
//                        newFragment = new FragmentMacLeanBJ(); //Replace this with your fragment class
//                        break;
//                    case "Joe Smith":
//                        newFragment = new FragmentMacLeanBJ(); //Replace this with your fragment class
//                        break;
//                    default:
//                        newFragment = new FragmentMacLeanBJ(); //BJs for default
//                }
//
//                //**********************************************************************************
//                // This code will replace the fragment on the content_main.xml with the new one.
//                //**********************************************************************************
//
//                ft = fm.beginTransaction();
//                ft.replace(R.id.fragment, newFragment);
//                ft.commit();
//            }
//        });
    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************
            Toast.makeText(getApplicationContext(),
                    "Hi, settings was chosen", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //nav camera
        } else if (id == R.id.nav_gallery) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************

            Toast.makeText(getApplicationContext(),
                    "hi, nav gallery was chosen.", Toast.LENGTH_SHORT).show();
            // Handle the camera action

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String message) {
        Log.d("bjtest", "interaction with fragment happened: "+message);
    }
}
